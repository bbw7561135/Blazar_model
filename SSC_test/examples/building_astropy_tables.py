from astropy.table import Table, Column
from astropy.io import ascii
from astropy import constants as const
from astropy import units as u
import numpy as np
import re
import naima

############################################# USIng Astropy ##############################################################

def astropy_data_table(energy, flux, flux_error_lo, flux_error_hi):
    data_table = Table()
    data_table['energy'] = Column(energy, unit = 'GeV', dtype = 'float64', description='energy')
    data_table['flux'] = Column(flux, unit = 'TeV^-1 cm^-2 s^-1', dtype = 'float64', description='flux')
    data_table['flux_error_lo'] = Column(flux_errl, unit = 'TeV^-1 cm^-2 s^-1', dtype = 'float64', description='flux_error_low')
    data_table['flux_error_hi'] = Column(flux_errh, unit = 'TeV^-1 cm^-2 s^-1', dtype = 'float64', description='flux_error_high')
    return data_table

############################################# OR SIMPLY USE NAIMA ####################################################
#Note: The parameters are not tunable according to the user's wish. Say one cannot add a description to the columns
#Which might not be absolutely necessary though! 

def naima_data_table(energy, flux, flux_error_lo, flux_error_hi):
    data_table_naima = naima.build_data_table(energy*u.GeV, 
                                              flux*u.Unit('TeV^-1 cm^-2 s^-1'), 
                                              flux_error_lo*u.Unit('TeV^-1 cm^-2 s^-1'), 
                                              flux_error_hi*u.Unit('TeV^-1 cm^-2 s^-1'))
    return data_table_naima

#####################################################################################################################

if __name__ == '__main__':
    text_data = open("energy_flux_ferrlow_ferrhi.txt","r")
    rows = text_data.readlines()
    flux = []
    flux_errl = []
    flux_errh = []
    energy = []

    for row in rows:
        entries = re.findall(r'\S+',row)
        if len(entries) == 4:
            flux.append(float(entries[1]))
            flux_errl.append(float(entries[2]))
            flux_errh.append(float(entries[3]))
            energy.append(float(entries[0]))

    data_table_naima = naima_data_table(energy, flux, flux_errl, flux_errh)
    data_table_astpy = astropy_data_table(energy, flux, flux_errl, flux_errh)
   
    #This ascii file will be data input to naima.
    data_table_astpy.write("data_table_vhe_astropy.dat", format='ascii.ecsv')
    data_table_naima.write("data_table_vhe_naima.dat", format='ascii.ecsv')   

    #data = Table.read('data_table_vhe.dat', format='ascii')

    #print("....................................................\n", data)
