"""
SSC Time-dependent model for AGN
@author: Simone Garrappa
"""

from numpy import log10,log
import numpy as np
from numpy import empty,zeros
from pylab import plot,xlabel,ylabel,show,ylim,figure,xlim
from scipy import pi,exp
from scipy import special as sp
import matplotlib as mp


#Costanti

N = 20 #numero di divisioni della griglia energie
s = 1.7 #indice spettrale
p = 1.7
lb = 0.075
Q0e = 1e-1   #iniezione costante
b = 1e-13
h=6.62606896e-27 # Planck constant in erg*s
c=2.99792458e10     # speed of light in cm/s
k=1.3806504e-16    #Boltzmann constant in erg/K
me = 9.1e-28
T = 53236
knor = 1.
B = 1. #magnetic field (Gauss)
Ub = B/(8.*pi)
s_T = 0.66524e-24 #sezione d'urto di thompson (cm^-2)
R = 1e16  #radius (cm)
ec = 4.81e-10   #electron charge (sC)
nu_B = ec*B/(2*pi*me*c)
nu_L = ec*B/(2*pi*me*c)
alpha = (p-1.)/2.
#tempi caratteristici
t_esc = 1.5*R/c
print t_esc
dt = 0.01*(R/c)
t1 = -4
t2 = -3
t3 = -2
t4 = -1
t5 =  1
#range dello spettro
gamma_min = 1e0
gamma_max = 1e5
#lgmin = log10(gmin)
#lgmax = log10(gmax)
#i = 0
U_rin = 0.
tau_c = Q0e*s_T*R
LEarray = np.zeros(N,float) #DEFINISCO ARRAY ENERGIE (A SPAZIATURA LOGARITMICA)

for j in range(0,N,1):
        LEarray[j] = gamma_min*(gamma_max/gamma_min)**((j-1.)/(N-2.))
print LEarray
#LogEarray = np.linspace(1.,4.,N)  #array logaritmico
#LEarray = 10**LogEarray             #array dei gamma degli elettroni
nu_Carray = 3./2.*(ec*B/(2.*pi*me*c))*(LEarray**2)
LNUarray = nu_L*LEarray**2                        #sono le frequenze dei fotoni
E_Carray = (h/(me*c**2))*nu_Carray
LXarray = LNUarray*(h/(me*c**2))                  #X dei fotoni
LZarray = LNUarray/(3*LEarray**2*nu_B)            #E' la variabile usata nella potenza del singolo elettrone
LNUCarray = (4./3.)*LNUarray*LEarray**2           #Array delle frequenze dei fotoni scatterati
LNUlimitarray = (3*me*c**2)/(4*h*LEarray)
#LNUClogarray = np.linspace(20.,30.,N)
#LNUCarray = 10**LNUClogarray
#print LNUCarray

m1 = np.zeros(N,float)
m2 = np.zeros(N,float)
m1 = m1 + 1
m2 = m2 + 2

plot(np.log10(LNUarray),m1)
plot(np.log10(LNUlimitarray),m2)
show()

####### CALCOLO L'EMISSIVITA' DEL SINGOLO ELETTRONE ############

k_es = 1./(4.*pi)
k_ps = 3*3**0.5*s_T*c*Ub/(pi*nu_B)


P_s = k_ps*LZarray**2*(sp.kv(4./3.,LZarray)*sp.kv(1./3.,LZarray) -3./5.*LZarray*((sp.kv(4./3.,LZarray))**2 -(sp.kv(1./3.,LZarray)**2)))
P_s_ap = 1.78*(LXarray**0.297)*exp(-LXarray)

#print P_s_ap - P_s



Ltarray = np.linspace(0.,1.,10000)  # DEFINISCO ARRAY TEMPORALE (LINEARE)




#Creazione degli array per le distribuzioni
ne = zeros(N,float)             #ARRAY PER I VALORI DELLA DISTRIBUZIONE DI ELETTRONI AL TEMPO t
nes = zeros(N,float)		#ARRAY PER I VALORI DELLA DISTRIBUZIONE DI ELETTRONI AL TEMPO t + Dt

ng = zeros(N,float)             #ARRAY PER I VALORI DELLA DISTRIBUZIONE DI FOTONI AL TEMPO t
ngs = zeros(N,float)            #ARRAY PER I VALORI DELLA DISTRIBUZIONE DI FOTONI AL TEMPO t + Dt

Q = zeros(N,float)              #ARRAY PER LA INIEZIONE DI ELETTRONI
S = zeros(N,float)              #ARRAY DIPENDENTE DEL SISTEMA


e_s = zeros(N,float)
E_s = zeros(N,float)
E_c = zeros(N,float)
Urad = zeros(N,float)


####################################################################################################
#################  CONDIZIONE INIZIALE SUGLI ELETTRONI#############################################################
####################################################################################################
#t = 0
def powerlaw(x,s):
        return Q0e*x**-s

for i in range(0,N,1):
        ts = 0
        gam = LEarray[i]      #VALORI DI GAMMA DA UTILIZZARE NELL'EQUAZIONE (RIFERITO AL BIN DI ENERGIA)

        ne[i] = powerlaw(gam,s)
        #Q[i] = Q0e # constant injection
        Q[i] = powerlaw(gam,s)
#print ne
'''
###########################INIEZIONE GAUSSIANA ###################################################
mu = 7e4
sig = 500
k_norm = 1e2
def gaussian(x, mu, sig):
    return (k_norm)*np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))
for j in range (0,len(LEarray)-1):
    ne[j] = gaussian(LEarray[j],mu,sig)
    Q[j] = gaussian(LEarray[j],mu,sig)
'''

#print LEarray

plot(np.log10(LEarray),np.log10((LEarray**2)*ne),".")
#plot(np.log10(LEarray[1:N-1]),np.log10(LEarray[1:N-1]**2)*ne[1:N-1])
show()

########################################################################################################
################# CONDIZIONE INIZIALE SUI FOTONI #######################################################
########################################################################################################
'''
def blackbody(x,Tcmb): # T is temperature (K)
    u_c= knor*8.*pi*(c**3)*(me**3)/(h**3)
    theta = me*(c**2)/(k*T)
    eb=(x**2)/(exp(x*theta)-1 )
    u_q=u_c*eb  # energy density
    return u_q


def intblackbody(y,T): # T is temperature (K)
    u_1= knor*8.*pi*(c**3)*(me**3)/(h**3)
    theta = me*(c**2)/(k*T)
    eb1=(y**3)/(exp(y*theta)-1 )
    u_2=u_1*eb1  # energy density
    return u_2

'''
'''
for j in range (0,len(LXarray)-1):
    ng[j] = blackbody(10**(LXarray[j]),Tcmb)
'''
'''
for j in range (1,len(LXarray)-1):
    ng[j] = intblackbody(10**(LXarray[j]),T)
    #print ng[j]
ng = ng +1e-5
plot(LXarray,np.log10(ng))
ylim(-5,20)
show()
'''

########DEFINIZIONE PARAMETRI COMPTON #############################
nu_C2 = (4./3.)*gamma_min**2*LNUarray[N-1]
nu_C3 = (4./3.)*gamma_max**2*LNUarray[0]
####################################################################################################
########### DEFINIZIONE DELLA MATRICE DEI COEFF. ###################################################
####################################################################################################

A = np.zeros((N,N),float)
s = np.zeros(N,float)
Urad1 = 0.
#definisco i valori dei coefficienti a t = 0

for i in range(0,N-1,1):
    gamma_p = 4./3.*(s_T/(me*c))*(Ub + Urad)*(LEarray[i])**2
    gamma_p12 = (LEarray[i+1] + LEarray[i])/2.
    print 'gamma_p12', gamma_p12
    gamma_m12 = (LEarray[i-1] + LEarray[i])/2.
    print 'gamma_m12', gamma_m12
    gammap_m12 = 4./3.*(s_T/(me*c))*Ub*(gamma_m12)**2
    gammap_p12 = 4./3.*(s_T/(me*c))*Ub*(gamma_p12)**2
	#print gammap_p12
    del_gamma = gamma_p12 - gamma_m12
    V2 = 1. + dt/t_esc + dt*gammap_m12/del_gamma               # aggiungere
    V3 =  -dt*gammap_p12/del_gamma
    s[i] = ne[i] + Q[i]*dt
	#print type(ne[i]),type(Q[i]),type(S[i])
    for j in range(0,N-1,1):
                if j == i:
                        A[i,j] = V2
                if j == i+1:
                        A[i,j] = V3
A[N-2,N-1] = 1e-12
A[N-1,N-1] = 1e-12
print 'printing Chang Cooper matrix'
print A



###############################################################################
############       SED          ###############################################
###############################################################################

#### NOTA #####################################################################
# Viene effettuato un ciclo temporale per calcolare l'evoluzione dello spettro#
# ai vari t, e viene quindi subito calcolato il valore dell'energia di        #
# breakdown. Si utilizza la forma analitica nota per valori di energia sopra  #
# il breakdown, mentre per il resto dello spettro viene risolta l'equazione   #
# differenziale relativa all'evoluzione dello spettro. E' stata utilizzata    #
# un'integrazione numerica con discretizzazione della derivata temporale e di #
# quella rispetto all'energia. Viene infine calcolata e plottata la SED a     #
# determinati t.                                                              #
###############################################################################



for j in range(0,302):  #deve diventare il ciclo di t!


    nes = np.linalg.solve(A,s)

    #check = np.allclose(np.dot(A, nes), s)
    #print check

    #s = nes + Q*dt
    #print S

    #print nes



    '''
    for i in range(len(LXarray)-2,0,-1):
            xb = 10**(LXarray[i])
            ngs[i] = ng[i] + h*((2.0/3.0)*lb*(b**-1.5)*(xb**-0.5)*ne[i])
    '''
    e_sin = 0
    ne,nes = nes,ne
    '''
    for i in range(0,N-12):
            for i1 in range (0,N-1,1):
                    gamma_p12 = (LEarray[i1+1] + LEarray[i1])/2.
                    gamma_m12 = (LEarray[i1-1] + LEarray[i1])/2.
                    del_gamma = gamma_p12 - gamma_m12
                    z = LNUarray[i]/(3.*LEarray[i1]**2*nu_B)
                    P_sa = k_ps*z**2*(sp.kv(4./3.,z)*sp.kv(1./3.,z) -3./5.*z*((sp.kv(4./3.,z))**2 -(sp.kv(1./3.,z)**2)))
                    e_sin += ne[i1]*P_sa*del_gamma
            E_s[i] = k_es*e_sin
            e_sin = 0.
    #print E_s
    E_s += 1e-30
    I_s = R*E_s         #syncrotron radiation field
    '''
    for i in range(0,N-1):
            gamma_p12 = (LEarray[i+1] + LEarray[i])/2.
	    gamma_m12 = (LEarray[i-1] + LEarray[i])/2.
            del_gamma = gamma_p12 - gamma_m12
            e_s[i] = ne[i]*P_s[i]*del_gamma
    E_s = k_es*e_s
    I_s = R*E_s         #syncrotron radiation field
    #E_sm = E_s*1e20

    '''
    for i in range(0,N-2):
            nu_p12 = (LNUarray[i+1] + LNUarray[i])/2.
	    nu_m12 = (LNUarray[i-1] + LNUarray[i])/2.
            del_nu = nu_p12 - nu_m12
            U_rin += I_s[i]*del_nu
    Urad = 4*pi*U_rin/c
    '''
    #print Urad



    #print I_s

    #g2ner = reversed(g2ne)
    Urad_1 = 0.
    for i0 in range(0,N-1,1):
            #print LNUarray[N-1] < LNUlimitarray[i0]
            if LNUarray[N-1]> LNUlimitarray[i0]:

                    for i01 in range(0,i0,1):
                            nu_p12 = (LNUarray[i01+1] + LNUarray[i01])/2.
                            nu_m12 = (LNUarray[i01-1] + LNUarray[i01])/2.
                            del_nu = nu_p12 - nu_m12
                            Urad_1 += (4.*pi/c)*del_nu*I_s[i01]
                            #print Urad_1
            if LNUarray[N-1]< LNUlimitarray[i0]:

                    for i02 in range(0,N-2,1):
                            nu_p12 = (LNUarray[i02+1] + LNUarray[i02])/2.
                            nu_m12 = (LNUarray[i02-1] + LNUarray[i02])/2.
                            del_nu = nu_p12 - nu_m12
                            Urad_1 += (4.*pi/c)*del_nu*I_s[i02]
                            #print Urad_1
            Urad[i0] = Urad_1
            Urad_1 = 0.
    #print 'U_rad is:', Urad

    for i in range(0,N-1,1):
        gamma_p = 4./3.*(s_T/(me*c))*(Ub + Urad[i])*(LEarray[i])**2
        gamma_p12 = (LEarray[i+1] + LEarray[i])/2.
        gamma_m12 = (LEarray[i-1] + LEarray[i])/2.
        gammap_m12 = 4./3.*(s_T/(me*c))*Ub*(gamma_m12)**2
        gammap_p12 = 4./3.*(s_T/(me*c))*Ub*(gamma_p12)**2
        #print gammap_p12
        del_gamma = gamma_p12 - gamma_m12
        V2 = 1. + dt/t_esc + dt*gammap_m12/del_gamma               # aggiungere
        V3 =  -dt*gammap_p12/del_gamma
        if j <=20:
                s[i] = ne[i] + Q[i]*dt
        else:
                s[i] = ne[i]
        #print type(ne[i]),type(Q[i]),type(S[i])
        for m in range(0,N-1,1):
                if m == i:
                        A[i,m] = V2
                if m == i+1:
                        A[i,m] = V3
        A[N-2,N-1] = 1e-12
        A[N-1,N-1] = 1e-12
    #print j
    #print A
    #plot(LEarray[1:N-1],g2ne)
    #print Urad
    #############CALCOLO L'INTEGRALE DELLA EMISSIVITA' COMPTON NEI VARI INTERVALLI DI FREQUENZA DEI FOTONI INCIDENTI ################
    '''
    for i in range(0,N-1,1):  #sto facendo il ciclo su tutti i nu_c
            ec_int = 0
            if LNUCarray[i]< nu_C2:             #RANGE 1
                    for i1 in range(0,N-1,1):   #integrale sul range 1 con gli opportuni estremi
                            if LNUarray[i1] < 3.*LNUCarray[i]/(4.*gamma_min**2):
                                    ec_int += E_s[i1]*LNUarray[i1]**(alpha-1.)
            if LNUCarray[i] > nu_C2 and LNUCarray[i] < nu_C3:   #RANGE 2
                    for i2 in range(0,N-1,1):
                            ec_int += E_s[i2]*LNUarray[i2]**(alpha-1)
            if LNUCarray[i] > nu_C3:
                    for i3 in range(N-1,0,-1):
                            if LNUarray[i3]> 3.*LNUCarray[i]/(4.*gamma_max**2):
                                    ec_int += E_s[i1]*LNUarray[i1]**(alpha-1.)
            E_c = ((4./3.)**(alpha-1))*tau_c*(LNUCarray**(-alpha))*ec_int
            #print ec_int
    #print E_c
    I_c = R*E_c
    '''
    E_c = ((4./3.)**(alpha-1.))/2.*tau_c*E_s*log(LNUarray[N-1]/LNUarray[0])

    I_c = R*E_c

    g2ne = np.log10(ne[1:N-1]*(LEarray[1:N-1])*(LEarray[1:N-1]))
    x2ng = np.log10(LNUarray[1:N-1]*E_s[1:N-1])
    nc2ng = np.log10(LNUCarray[1:N-1]*E_c[1:N-1])
    #print tau_c*log(LNUarray[N-1]/LNUarray[0])

    '''
    if j == 100:
        plot(np.log10(LEarray[1:N-1]),g2ne,".")
        #print g2ne
    '''
    figure(1)
    if j == 100:
        plot(log10(LEarray[1:N-1]),g2ne,"b")
        print "100!"
    if j == 150:
        plot(log10(LEarray[1:N-1]),g2ne,"r")
        print "500!"
    if j == 200:
        plot(log10(LEarray[1:N-1]),g2ne,"g")
        print "1000!"
    if j == 250:
        plot(log10(LEarray[1:N-1]),g2ne,"c")
        print "1500!"
    if j == 300:
        plot(log10(LEarray[1:N-1]),g2ne,"m")
        print "2000!"
    xlabel(r'$\lg(\gamma)$')
    ylabel(r'$\lg(\gamma^{2}n_{e}$)')


    figure(2)

    if j == 100:
            plot(log10(LNUarray[1:N-1]),x2ng,"b")
            print "100!"
    if j == 150:
        plot(log10(LNUarray[1:N-1]),x2ng,"r")
        print "500!"
    if j == 200:
        plot(log10(LNUarray[1:N-1]),x2ng,"g")
        print "1000!"
    if j == 250:
        plot(log10(LNUarray[1:N-1]),x2ng,"c")
        print "1500!"
    if j == 300:
        plot(log10(LNUarray[1:N-1]),x2ng,"m")
        print "1900!"

    if j == 100:
            #print nc2ng
            plot(log10(LNUCarray[1:N-1]),nc2ng,"b")
    if j == 150:
            plot(log10(LNUCarray[1:N-1]),nc2ng,"r")
    if j == 200:
            plot(log10(LNUCarray[1:N-1]),nc2ng,"g")
    if j == 250:
            plot(log10(LNUCarray[1:N-1]),nc2ng,"c")
    if j == 300:
            plot(log10(LNUCarray[1:N-1]),nc2ng,"m")

####################### FINE SED ###################################################

########

###########LABEL ELETTRONI###############
#xlabel(r'$\lg(\gamma)$')
xlabel(r'$\lg(\nu)$')
#ylabel(r'$\lg(n_e$)')
ylabel(r'$\lg({\nu}F_{\nu}$)')
#xlim(12,24)
#########################################

###########LABEL FOTONI##################
#xlabel(r'$\lg(x)$')
#ylabel(r'$\lg({x^2}n_\gamma$)')
#########################################
show()
