from __future__ import division
from math import pi
import numpy as np
from astropy import constants as const
import astropy.units as u
import naima

class SSC_model:
    '''
    This class creates an object which contains the particle spectra of electrons
    and photons in case of SSC process in AGN.
    Reference is Chiaberge & Ghisellini (1998) MNRAS, 306,551 (1999)
    '''
    sigma_T =  const.sigma_T.to('cm2').value
    c = const.c.to('cm/s').value
    m_e = const.m_e.to('g').value
    E_rest = m_e*c**2

    def __init__(self, time_grid, gamma_grid, emission_region, injected_spectrum):
        '''
        This is the constructor of our SSC process with
        * time_grid = dict(minimum, maximum time, binning)
        initializing the time grid
        * gamma_grid = dict(minimum, maximum Lorentz factor, binning)
        initializing the Lorentz factor grid
        * emission_region dict(radius, magnetic field, escaping time)
        with parameters describing the emission region, escaping time in order of R/c
        * injected_spectrum dict(norm, index)
        initializing the injected spectrum (a powerlaw for now).
        '''
        self.crossing_time = emission_region['R']/const.c.to('cm/s').value

        # time grid attributes definition
        self.time_min = time_grid['time_min']*self.crossing_time
        self.time_max =  time_grid['time_max']*self.crossing_time
        self.time_bins = time_grid['time_bins']
        self.delta_t = (self.time_max - self.time_min)/self.time_bins
        self.time_grid = np.linspace(self.time_min, self.time_max, self.time_bins)

        # gamma grid attributes defintion
        self.gamma_min = gamma_grid['gamma_min']
        self.gamma_max = gamma_grid['gamma_max']
        self.gamma_bins = gamma_grid['gamma_bins']
        # following Eq.(5) of the Reference, LorentzFactor grid has to be logspaced
        self.gamma_grid = np.logspace(np.log10(self.gamma_min), np.log10(self.gamma_max), self.gamma_bins)
        # energy grid for use in calculating the synchrotron emission
        self.energy_grid = self.gamma_grid*self.E_rest

        # emission regions attributes definitions
        self.R = emission_region['R'] # in cm
        self.B = emission_region['B'] # in Gauss
        self.U_B = self.B/(8.*pi) # magnetic field density
        self.t_esc = emission_region['t_esc']*self.crossing_time
        self.U_rad = 0. # case of simple synchrotron cooling

        # injected spectrum attributes
        self.inj_spectr_norm = injected_spectrum['norm']
        self.inj_spectr_index = injected_spectrum['alpha']


    @property
    def gamma_grid_midpts(self):
        '''
        will return us the Lorentz factor grid midpoints \gamma_{j \pm 1/2}
        '''
        return (self.gamma_grid[1:]*self.gamma_grid[:-1])**0.5

    @property
    def delta_gamma(self):
        '''
        will return us the \Delta \gamma as defined after Eq.(5)
        '''
        return self.gamma_grid_midpts[1:] -  self.gamma_grid_midpts[:-1]

    @property
    def N_e_inj(self):
        # particle distribution of injected electrons
        return np.array([self.inj_spectr_norm*gamma**self.inj_spectr_index for gamma in self.gamma_grid])

    def cool_rate(self, U_B, U_rad, gamma):
        '''
        cool rate from Eq.(2)
        '''
        return 4/3 * self.sigma_T/(self.m_e*self.c) * (U_B + U_rad) * gamma**2

    @property
    def ChaCoo_tridiag_matrix(self):
        '''
        Implementing tridiagonal matrix of Eq.(9)
        '''
        # the dimension of our problem is
        N = len(self.delta_gamma)
        ChaCoo_matrix = np.zeros((N,N), float)
        # loop on the energy to fill the matrix
        for i in range(N):
            gamma_minus_half = self.gamma_grid_midpts[i]                        # \gamma_{j-1/2} of Reference
            gamma_plus_half = self.gamma_grid_midpts[i] + self.delta_gamma[i]   # \gamma_{j+1/2} of Reference
            # Eq.s (10) of Reference
            V2 = 1 + self.delta_t/self.t_esc + \
                     (self.delta_t * self.cool_rate(self.U_B, self.U_rad, gamma_minus_half))/self.delta_gamma[i]
            V3 = - (self.delta_t * self.cool_rate(self.U_B, self.U_rad, gamma_plus_half))/self.delta_gamma[i]
            # let's loop on another dimension to fill the diagonal of the matrix
            for j in range(N):
                if j == i:
                    ChaCoo_matrix[i,j] = V2
                if j == i+1:
                    ChaCoo_matrix[i,j] = V3

        return ChaCoo_matrix

    @property
    def constant_injection(self):
        '''
        will initialize an array for costant injection
        '''
        return np.array([self.inj_spectr_norm for gamma in self.gamma_grid])

    @property
    def powerlaw_injection(self):
        '''
        will initialize an array for power-law injection
        '''
        return self.N_e_inj


    def evolve_synchro_only(self):
        '''
        Evolving injected spectrum solving iteratively Eq.(9)
        '''
        N_e = self.N_e_inj[1:-1] + self.powerlaw_injection[1:-1]
        # time grid loop
        for time in self.time_grid:
            N_e_tmp = np.linalg.solve(self.ChaCoo_tridiag_matrix, N_e)
            # swap
            N_e, N_e_tmp = N_e_tmp, N_e

        return N_e

    def synchro_emission(self):
        '''
        returns synchrotron emission
        '''
        # we will feed neima with a customized function
        TableModel = naima.models.TableModel(self.energy_grid[1:-1]*u.erg, self.evolve_synchro_only()/u.erg)
        SYN = naima.models.Synchrotron(TableModel, B=self.B*u.G) # remember naima's funcs need units
        return SYN
