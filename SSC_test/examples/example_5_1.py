from scipy import integrate
import numpy as np

def f(x):
	return x**4 - 2*x +1

N = 1000 #int(1e6)
a = 0.0
b = 2.0
h = (b-a)/N

s = .5*f(a) + .5*f(b)

for k in range(1,N):
	s += f(a+k*h)

xarray = np.linspace(a, b, N)
f_sampled = np.array([f(x) for x in xarray]) 

print 'Trapezoidal rule: ', (h*s)
print 'Simpson rule: ', integrate.simps(f_sampled, xarray)
