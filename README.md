# Blazar model

This is a pyhton code developed for the evaluation of non thermal emission of ***Blazar***.
For now only a SSC model is developed, later on we will implement external Compton and hadronic scenarios. 

To use the code you need python (code is written with *python 2.7*), the following packages are also required
These are the ones I'm actually using on Red Hat Enterprise Linux 6.8  
  `numpy==1.10.2`     
  `scipy==0.16.0`     
  `astropy==1.1.2`     
  `matplotlib==1.4.3`    
  `naima==0.8`   
last one - naima - is the most important.
Naima and all of its dependencies can be installed in an Anaconda distribution through the Astropy conda channel:   
    `$ conda config --add channels astropy`    
    `$ conda install naima`    
For more info http://naima.readthedocs.io/en/latest/index.html.

## Structure
For now there is just one folder with Synchrotron Self Compton implementation

### SSC test   
Is the directory containing the macro for the synchrotron self compton spectrum evaluation, go there for the next readme...

B-)

